## D_KRYPTOGRAFIE_PGP_ANNEX



### Aufgabe 1
 
Man kann einen Public-Key bei einer Zertifizierungsstelle verifizieren.


### Aufgabe 2

Die Public Key Infrastructure (PKI) ist ein System zur Verwaltung und Sicherung von Public Keys.



### Aufgabe 3

- Certification Authority (CA): Eine Zertifizierungsstelle ist eine vertrauenswürdige Einrichtung, die digitale Zertifikate ausstellt und verifiziert. Sie überprüft die Identität von Personen oder Organisationen und bindet ihre öffentlichen Schlüssel in digitale Zertifikate ein.

- Trust-Center (TC): Ein Trust-Center ist eine Organisation oder Einrichtung, die Vertrauensdienste bereitstellt und die Integrität, Authentizität und Vertraulichkeit von elektronischen Kommunikationen sicherstellt. Es kann eine Zertifizierungsstelle (CA) sein oder andere Sicherheitsdienste anbieten, um das Vertrauen in digitale Transaktionen zu gewährleisten.





# Reflexion zum Tag

## Ziele/Resultate:
- Erfolgreiche Bearbeitung verschiedener Themen im Bereich der Public-Key-Infrastruktur (PKI) und E-Mail-Verschlüsselung.
- Verständnis für die Bedeutung und Funktion von Certification Authorities (CAs) und Trust-Centern entwickelt.Vigenère-Verschlüsselung, XOR-Stromchiffre, Diffie-Hellman-Protokoll und RSA.
- Unterschiede zwischen verschiedenen Zertifikatstypen und Verschlüsselungsverfahren wie OpenPGP und X.509 verstanden.
- Erfolgreiche Konfiguration und Nutzung von Tools wie GPG4WIN/Kleopatra und Thunderbird zur Verschlüsselung und Signierung von E-Mails.

  
## Knacknüsse:
- Sicherstellung der Authentizität von Public Keys und Zertifikaten.
- Verständnis für die Einrichtung und Nutzung von Verschlüsselungssoftware wie GPG4WIN und Thunderbird.
- Überwindung von möglichen Hindernissen bei der Konfiguration von E-Mail-Verschlüsselung.

  
## Tools:
- Verwendung von GPG4WIN/Kleopatra und Thunderbird zur Erzeugung und Verwaltung von Schlüsselpaaren sowie zur Verschlüsselung und Signierung von E-Mails.
- Verwendung von GPG4WIN/Kleopatra und Thunderbird zur Erzeugung und Verwaltung von Schlüsselpaaren sowie zur Verschlüsselung und Signierung von E-Mails.

  
## Erkenntnisse:
- Wichtigkeit der sicheren Kommunikation und Datenübertragung durch Verschlüsselungstechniken wie OpenPGP und SSL/TLS (HTTPS) besser verstanden.
- Notwendigkeit von Public-Key-Infrastrukturen (PKIs) für die Verwaltung von Zertifikaten und Schlüsseln erkannt.
- Herausforderungen bei der Verifizierung von Zertifikaten und Public Keys sowie bei der Einrichtung und Nutzung von Verschlüsselungssoftware erfahren und gemeistert.


