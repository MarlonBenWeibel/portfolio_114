## C_BILDER_CODIEREN_ANNEX


### Aufgabe 1

• RGB(255, 255, 255) entspricht Farbe: #ffffff 
<br>
• RGB(0,0,0) entspricht Farbe: #000000
<br>
• RGB(252,178,91) entspricht Farbe: #fcb25b
<br>
• #FF0000 entspricht Farbe: rgb(255, 0, 0)
<br>
• #00FF00 entspricht Farbe: rgb(0, 255, 0)
<br>
• #0000FF entspricht Farbe: rgb(0, 0, 255)
<br>
• #FFFF00 entspricht Farbe: rgb(255, 255, 0)
<br>
• #00FFFF entspricht Farbe: rgb(0, 255, 255)
<br>
• #FF00FF entspricht Farbe: rgb(255, 0, 255)
<br>
• #000000 entspricht Farbe: rgb(0, 0, 0)
<br>
• #FFFFFF entspricht Farbe: rgb(255, 255, 255)
<br>
• #00BC00 entspricht Farbe: rgb(0, 188, 0)



### Aufgabe 2

• CMYK(0%, 100%, 100%, 0%) entspricht Farbe: rgb(255, 0, 0) #ff0000
<br>
• CMYK(100%, 0%, 100%, 0%) entspricht Farbe: rgb(0, 255, 0) #00ff00
<br>
• CMYK(100%, 100%, 0%, 0%) entspricht Farbe: rgb(0, 0, 255) #0000ff
<br>
• CMYK(0%, 0%, 100%, 0%) entspricht Farbe: rgb(255, 255, 0) #ffff00
<br>
• CMYK(100%, 0%, 0%, 0%) entspricht Farbe: rgb(0, 255, 255) #00ffff
<br>
• CMYK(0%, 100%, 0%, 0%) entspricht Farbe: rgb(255, 0, 255) #ff00ff
<br>
• CMYK(100%, 100%, 100%, 0%) entspricht Farbe: rgb(0, 0, 0) #000000
<br>
• CMYK(0%, 0%, 0%, 100%) entspricht Farbe: rgb(0, 0, 0) #000000
<br>
• CMYK(0%, 0%, 0%, 0%) entspricht Farbe: rgb(255, 255, 255) #ffffff
<br>
• CMYK(0%, 46%, 38%, 22%) entspricht Farbe: rgb(199, 107, 123) #c76b7b



### Aufgabe 3

Der theoretische Speicherbedarf eines unkomprimierten RGB-Bildes mit einer Größe von 640 x 480 Pixeln und 8 Bit Auflösung pro Farbkanal beträgt:

640 Pixel * 480 Pixel * 3 Farbkanäle * 8 Bit = 7,372,800 Bit

Um den Speicherbedarf in Byte zu berechnen, teilen wir den Speicherbedarf in Bit durch 8:

7,372,800 Bit / 8 = 921,600 Byte


### Aufgabe 4

Für das Hintergrundbild würde ich mich für ein verlustfreies Bildformat wie PNG entscheiden. PNG (Portable Network Graphics) unterstützt Transparenz, was nützlich sein könnte, wenn die Textur keine vollständig undurchsichtige Deckkraft hat. Zudem bietet PNG eine gute Kompression ohne Qualitätsverlust, was für das Kacheln der Textur von Vorteil ist.

Für das Firmenlogo, das schwarzweiß ist, wäre ein Bildformat wie PNG oder GIF geeignet. Beide Formate unterstützen Transparenz und bieten verlustfreie Kompression.


### Aufgabe 5

Für ein 30-Zoll-Display mit einem Seitenverhältnis von 16:10 und einer Pixeldichte von 100 ppi (Pixel pro Zoll) ergibt sich:

Horizontal: 30 Zoll * (16 / 26) = 1875 Pixel
Vertikal: 30 Zoll * (10 / 26) = 1153 Pixel

Die Pixelauflösung beträgt also horizontal 1875 Pixel und vertikal 1153 Pixel.


### Aufgabe 6

Um die Grösse des Fotos in Zentimetern zu berechnen, teilen wir einfach die Anzahl der Pixel durch die Druckauflösung in Pixel pro Zoll (dpi) und multiplizieren das Ergebnis mit 2.54 (um von Zoll in Zentimeter umzurechnen).

2000 Pixel / 600 dpi * 2.54 cm/Zoll = 8.47 cm

Das quadratische Foto wird also ungefähr 8.47 cm auf jeder Seite gross sein.



### Aufgabe 7

Das HD1080p50-Format hat eine Auflösung von 1920x1080 Pixeln und eine Bildwiederholfrequenz von 50 Hz. Bei True-Color-Farbauflösung bedeutet das, dass für jeden Pixel 24 Bit benötigt werden (8 Bit pro Farbkanal für Rot, Grün und Blau).

Der Speicherbedarf für ein einzelnes unkomprimiertes Bild im HD1080p50-Format beträgt:

1920 Pixel * 1080 Pixel * 24 Bit = 49,766,400 Bit

Um den Speicherbedarf in Byte zu berechnen, teilen wir durch 8:

49,766,400 Bit / 8 = 6,220,800 Byte

Der Speicherbedarf beträgt also etwa 6,221 Megabyte (MB).



### Aufgabe 8

Um den Speicherbedarf für das Video zu berechnen, multiplizieren wir die Bitrate mit der Spieldauer:

20 Mbit/s * 3 Minuten = 60 Megabit

Um das Ergebnis in Megabyte umzuwandeln, teilen wir durch 8:

60 Megabit / 8 = 7,5 Megabyte

Das Video würde also ungefähr 7,5 Megabyte Speicherplatz pro Minute benötigen, insgesamt also etwa 22,5 Megabyte für eine Spieldauer von 3 Minuten.


### Aufgabe 9

RAW:

- Unkomprimiertes Format, speichert Rohdaten direkt von der Kamera.
- Enthält alle Aufnahmeinformationen und bietet maximale Flexibilität für die Nachbearbeitung.
- Erfordert spezielle Software zur Bearbeitung.


JPG:

- Komprimiertes Format, verliert etwas Bildqualität.
- Geringere Dateigröße, einfacher zu teilen und zu drucken.
- Gut für den sofortigen Gebrauch ohne umfangreiche Bearbeitung.


Verwendungszwecke:

- RAW für professionelle Fotografen und umfangreiche Bearbeitung.
- JPG für schnelles Teilen, Drucken und den allgemeinen Gebrauch ohne aufwändige Bearbeitung.


### Aufgabe 10


Die technischen Vorgaben für das Veröffentlichen eines Gameplay-Videos auf YouTube sind:

- Format: YouTube unterstützt eine Vielzahl von Videoformaten wie MP4, AVI, MOV und mehr. Das empfohlene Format ist jedoch MP4.
- Bildrate: Ideal ist eine Bildrate von 24, 25, 30, 48, 50 oder 60 FPS (Frames pro Sekunde).
- Farbauflösung: YouTube unterstützt eine maximale Auflösung von 8K (7680 x 4320 Pixel).
- Video-Codec: Häufig verwendet wird der H.264-Codec für das Video.
- Audio-Codec: Der AAC-Codec wird oft für das Audio verwendet.

Rechtliche Einschränkungen könnten bestehen, wenn das Gameplay-Material urheberrechtlich geschützte Inhalte enthält, wie z. B. Musik, Grafiken oder Dialoge aus Videospielen oder anderen Quellen. Es ist wichtig sicherzustellen, dass Sie über die entsprechenden Rechte oder Lizenzen verfügen, um solche Inhalte zu verwenden, um Urheberrechtsverletzungen zu vermeiden.



### Aufgabe 11

- Interlaced-Modus: Hier werden die Bildinformationen in zwei Halbbildern dargestellt, wobei zuerst die geraden und dann die ungeraden Zeilen angezeigt werden. Dies führt zu einem schnelleren Bildaufbau, kann jedoch zu Bildverzerrungen und Flimmern führen, insbesondere bei schnellen Bewegungen.

- Progressive-Modus: Hier werden die Bildinformationen Zeile für Zeile in aufeinanderfolgender Reihenfolge dargestellt. Dies bietet eine bessere Bildqualität und Klarheit, insbesondere bei schnellen Bewegungen, da es kein Flimmern oder Bildverzerrungen gibt. Der gesamte Bildaufbau erfolgt in einem Durchgang, was zu einer glatteren Darstellung führt.


### Aufgabe 12

Artefakte sind unerwünschte Anomalien in digitalen Medien. Hier sind einige Arten:

Bildartefakte:
- Blockartefakte
- Rauschartefakte
- Kantenschärfungsartefakte
- Moiré-Muster

Audioartefakte:
- Rauschen
- Knistern und Knacken
- Kompressionsartefakte

### Aufgabe 13

Datenrate = Auflösung (in Pixel) * Bildrate (in Bildern pro Sekunde) * Farbauflösung (in Bit pro Pixel) / 1,000,000,000

Für HD1080i50 ohne Unterabtastung und 8 Bit Auflösung pro Farbkanal ergibt sich:

Auflösung: 1920 Pixel * 1080 Pixel
Bildrate: 50 Bilder pro Sekunde
Farbauflösung: 8 Bit pro Pixel

Datenrate = (1920 * 1080 * 50 * 8) / 1,000,000,000 = 8.2944 Gbps

Die Datenrate für die Übertragung beträgt also etwa 8,2944 Gbps.


### Aufgabe 14

Die Datenrate für unkomprimiertes HD1080i50-Video beträgt etwa 8,2944 Gbps (wie in der vorherigen Frage berechnet).

Die Speicherkapazität einer DVD-5 beträgt 4,7 GB, was ungefähr 37,6 Gigabit entspricht.

Um die Minuten zu berechnen, teilen wir die Speicherkapazität der DVD-5 durch die Datenrate des Videos:

37.6
 Gigabit
8.2944
 Gbps
≈
4.53
 Minuten
8.2944 Gbps
37.6 Gigabit
​
 ≈4.53 Minuten

Also wäre eine DVD-5 nach ungefähr 4,53 Minuten unkomprimierten HD1080i50-Videos voll.


### Aufgabe 15

Ein Codec (Codierer/Decodierer) komprimiert und dekomprimiert digitale Daten wie Audio oder Video. Ein Mediencontainer ist eine Dateistruktur, die verschiedene Arten von Mediendaten und Metadaten zusammenfasst und speichert. Der Codec behandelt die Daten, während der Container die Struktur bereitstellt, um sie zu speichern.


### Aufgabe 16

a. Ein AD-Wandler wandelt analoge Signale wie Spannungen oder Ströme in digitale Signale um. Das ist wichtig, weil viele moderne Geräte und Systeme nur mit digitalen Signalen arbeiten können.

b. Bei der Umwandlung von Analog zu Digital geht etwas an Genauigkeit verloren. Das passiert, weil das analoge Signal in diskrete digitale Werte umgewandelt wird. Je größer die Anzahl der Bits des AD-Wandlers ist, desto genauer ist die Umwandlung, aber es gibt immer einen gewissen Verlust an Detailtreue.

c. Eine höhere Samplingrate bedeutet, dass mehr Messungen pro Sekunde durchgeführt werden. Dadurch können feinere Details im Signal erfasst werden. Eine niedrigere Samplingrate kann zu einem Verlust von Details führen. Also, eine höhere Samplingrate liefert eine genauere Darstellung des Originalsignals.





# Reflexion zum Tag

## Ziele/Resultate:
- Untersuchung und Erklärung verschiedener Konzepte im Bereich der digitalen Medien, einschließlich Kodierung und Datenübertragung.
- Klarheit über die Unterschiede zwischen Codecs und Mediencontainern.
- Erfolgreiche Berechnung von Datenraten und Speicherbedarf für verschiedene digitale Medienformate.
- Erklärung von Konzepten wie AD-Wandlung und Samplingraten mit klaren und verständlichen Beispielen.
  
## Knacknüsse:
- Sicherstellung, dass die Antworten präzise und auf den Punkt gebracht sind, während gleichzeitig relevante Informationen nicht ausgelassen werden.
- Vereinfachung von technischen Konzepten, um sie verständlich zu erklären, ohne dabei wichtige Details zu verlieren.

  
## Tools:
- Notizbuch für Notizen und Überlegungen.
- Gelegentliche Online-Recherche für zusätzliche Klarheit bei komplexen Konzepten.
- Interaktive Tools für praktische Beispiele und Berechnungen.
  
## Kommandos:
- Keine spezifischen Kommandos verwendet.