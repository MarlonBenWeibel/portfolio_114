## C_BILDER_KOMPRIMIEREN_ANNEX


### Aufgabe 1
 
<br>
- YCbCr: Y=1, Cb=0, Cr=0
<br>
- YCbCr: Y=0, Cb=0, Cr=0
<br>
- Blau
<br>
- Orange
<br>
- Rot
<br>
- Blau
<br>
- Grün
<br>



### Aufgabe 2

Grauwert= 
3/R+G+B
​
 

Für das Hellblau mit den Werten 

R=33, G=121, B=239 in 8 Bit pro Farbkanal:

Grauwert=33+121+239/3=393/3=131




### Aufgabe 3

Bei Subsampling 4:1:1 werden für jede Gruppe von 4 Pixeln nur die Farbinformationen des ersten Pixels beibehalten, während die Helligkeitsinformationen für alle 4 Pixel gespeichert werden.

Das bedeutet, dass für jede Gruppe von 4 Pixeln nur 1/4 der Farbinformationen gespeichert werden im Vergleich zu einem Bild ohne Subsampling.

Daher beträgt die Speichereinsparung 75%.


### Aufgabe 4

a. Die Umwandlung vom RGB- in den YCbCr-Farbraum spart Speicherplatz, da der YCbCr-Farbraum Helligkeitsinformationen von Farbinformationen trennt und so eine effizientere Kompression ermöglicht.

b. Moderne Beamer können Bilder im YCbCr-Farbbereich darstellen, da sie verschiedene Farbräume unterstützen.

c. Es gibt verschiedene Methoden, um ein Farbbild in ein Graustufenbild umzuwandeln, darunter die Berechnung des Durchschnitts der RGB-Werte jedes Pixels oder die Gewichtung der RGB-Werte basierend auf der menschlichen Wahrnehmung von Helligkeit.

d. Der Grünanteil hat bei der Umwandlung eines Farbbildes in ein Graustufenbild am meisten Gewicht, da das menschliche Auge für grüne Farben am empfindlichsten ist, was sich in der Gewichtung der RGB-Werte widerspiegelt.


### Aufgabe 5

a. 4:1:1-Subsampling reduziert die Farbinformationen im Vergleich zu 4:4:4-Subsampling. Dies führt zu einer Speichereinsparung, ohne die Bildschärfe direkt zu beeinflussen.

b. Bei 4:1:1-Unterabtastung werden die Farbinformationen im Vergleich zur vollständigen RGB-Darstellung reduziert. Für jede Gruppe von vier Pixeln werden nur ein Farbwert für die Helligkeit und je ein Farbwert für die Chrominanz gespeichert.

Da das Bild eine Kantenlänge von 1000 Pixeln hat, ergibt sich eine Fläche von 1000 * 1000 = 1.000.000 Pixel.

Bei vollständiger RGB-Darstellung werden für jeden Pixel 24 Bit benötigt (8 Bit pro Farbkanal).
Bei 4:1:1-Unterabtastung werden für jede Gruppe von vier Pixeln 32 Bit benötigt (8 Bit für die Helligkeit und je 8 Bit für die Chrominanz).
Daher ergibt sich die Speicherplatzersparnis wie folgt:

Vollständige RGB-Darstellung: 1.000.000 Pixel * 24 Bit = 24.000.000 Bit
4:1:1-Unterabtastung: (1.000.000 Pixel / 4) * 32 Bit = 8.000.000 Bit
Die Speicherplatzersparnis beträgt somit 24.000.000 Bit - 8.000.000 Bit = 16.000.000 Bit.


### Aufgabe 6

a. Der erste Schritt bei der JPG-Komprimierung besteht darin, das Bild in den Frequenzbereich durch die Anwendung der diskreten Kosinustransformation (DCT) zu transformieren.

b. Die DCT-Transformation führt zu einer Datenreduktion, indem sie redundante Informationen im Bild entfernt, wobei hochfrequente Komponenten stärker reduziert werden als niederfrequente Komponenten.

c. Block-Artefakte können bei sehr starker Bildkomprimierung auftreten und sind sichtbare Unregelmäßigkeiten oder Muster, die durch die Aufteilung des Bildes in Blöcke und separate Komprimierung entstehen.



### Aufgabe 7

a. Bei der Intraframe-Komprimierung erfolgt die Datenreduktion innerhalb eines Einzelbildes, während bei der Interframe-Komprimierung die Datenreduktion zwischen den Bildern erfolgt.

b. Die Potenzial zur Datenreduktion bei Interframe-Komprimierung hängt von der Art der Bewegung und den Veränderungen im Bildinhalt ab.

d. Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung bestehen darin, dass beide darauf abzielen, nur die Unterschiede oder Änderungen zu speichern, um Speicherplatz zu sparen.






# Reflexion zum Tag

## Ziele/Resultate:
- Erfolgreiche Bearbeitung verschiedener Aufgaben im Bereich der digitalen Bildkompression, einschliesslich Umwandlung von Farbräumen, Subsampling und JPEG-Komprimierung.
- Klarheit über die Funktionsweise und Anwendungen von Konzepten wie YCbCr-Farbraum, Subsampling und DCT-Transformation erreicht.
- Effektive Berechnung von Speichereinsparungen durch Subsampling und Komprimierungsalgorithmen.

  
## Knacknüsse:
- Sicherstellung der Kürze bei der Beantwortung der Aufgaben, um präzise und dennoch vollständige Antworten zu liefern.
- Klare Darstellung technischer Konzepte, um ein ausgewogenes Verhältnis zwischen Verständlichkeit und Tiefe zu erreichen.

  
## Tools:
- Notizbuch für Notizen und Überlegungen.
- Gelegentliche Online-Recherche für zusätzliche Klarheit bei komplexen Konzepten.
- Verwendung von interaktiven Tools für praktische Beispiele und Berechnungen.
  
## Kommandos:
- Keine spezifischen Kommandos verwendet.