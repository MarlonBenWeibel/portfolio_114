## B_DATEN_CODIEREN_TXT_ANNEX


### Aufgabe 1

Baumstrukturen kommen auch in anderen Bereichen der IT vor, wie z.B. bei der Datenstruktur von Dateisystemen, der Organisation von Datenbanken und der Verarbeitung von XML- oder HTML-Dokumenten. Ein binärer Baum unterscheidet sich von einem nicht binären Baum dadurch, dass jeder Knoten höchstens zwei Kinder haben kann.


### Aufgabe 2

Huffman-Code für das Wort Schmettering inkl.Codetabelle:

'S': 1
'c': 1
'h': 1
'm': 1
'e': 3
't': 3
'r': 1
'l': 2
'i': 1
'n': 1
'g': 1
's': 1
'f': 1
'ü': 1

![Alt text](image.png)


"Schmetterlingsflügel" in Binärdarstellung lautet entsprechend:

 1100111110110010111100010010100010100001001011001010101001010011010011001011010010101010010101101001001001011010111010


### Aufgabe 3

Bei einem Farbbild müssten benachbarte Pixel mit ähnlichen Farben zusammengefasst werden, z.B. 11xGrün, 6xBlau, 3xWeiss usw. Die Bitbreite für die Codierung hängt von der Anzahl der Farben im Bild ab und kann je nach Anwendung variieren. Wenn das Bild nur aus einer Farbe besteht, wäre eine Bitbreite von 1 ausreichend.


### Aufgabe 4

Die Grafik stellt ein Muster von schwarzen und weissen Pixeln dar, das nicht weiter spezifiziert ist.


### Aufgabe 5

a. Die LZW-Codierung für das Wort "ANANAS" wäre wie folgt:

A: 65
N: 78
A: 65
N: 78
A: 65
S: 83
Die Codierung würde dann folgendermaßen aussehen: 65 78 256 83

b. Die Dekomprimierung des erhaltenen LZW-Codes "ERDBE<256>KL<260>" ist nicht möglich, da die verwendeten Zeichen nicht bekannt sind. Der Code "<256>" und "<260>" weisen auf eine Indexierung hin, die über den im ursprünglichen Codierungsschema definierten Bereich hinausgeht.


### Aufgabe 6

a. Die BWT-Transformation für das Wort "ANANAS" wäre:

ANANAS
NANASA
NASANA
ASANAN
SANANA
ANANAS
Die letzte Spalte nach der Sortierung ist "SANANA".

b. Das Wort, das sich hinter dem Code "IICRTGH6" in der Burrows-Wheeler-Transformation verbirgt, ist "ANANAS". 




### Aufgabe 7

Weitere Verfahren, bei denen verlustlos komprimiert wird, sind:

Run-Length-Encoding (RLC)
Huffman-Codierung

Daten, die überhaupt verlustlos komprimiert werden sollten, sind solche, bei denen keine Information verloren gehen darf und die originalen Daten exakt wiederhergestellt werden müssen. Dazu gehören:

Textdokumente
Programmcode (z.B. Java-Sourcecode)
Tabellarische Daten (z.B. CSV-Dateien)

Wenn man einen Brief oder einen Java-Sourcecode verlustbehaftet komprimieren würde, könnte es passieren, dass wichtige Informationen verloren gehen oder dass der Code nicht mehr ausführbar ist. Dies könnte zu Fehlfunktionen oder Fehlinterpretationen führen und somit die Qualität oder Funktionalität der Daten beeinträchtigen.



# Reflexion zum Tag

## Ziele/Resultate:
- Intensive Beschäftigung mit verschiedenen Themen im Bereich der Datenkodierung.
- Verständnis der Unterschiede zwischen verschiedenen Kodierungsformaten wie ASCII, UTF-8, UTF-16.
- Erfolgreiche Lösung von Aufgaben zu QR-Codes und anderen Komprimierungsalgorithmen.
  
## Knacknüsse:
- Verständnis komplexer Konzepte wie der Burrows-Wheeler-Transformation und dem LZW-Verfahren.

  
## Tools:
- Notepad
- Symbl.cc (für Unicode-Tabelle)
- QR-Code-Generatoren
  
## Kommandos:
- Keine spezifischen Kommandos verwendet.