## A_DATEN_CODIEREN_NUM_ANNEX


### Aufgabe 1

| Dezimal | Hexadezimal | Binär       | Binär       | Binär       | Binär       |
|---------|-------------|-------------|-------------|-------------|-------------|
| 0       | 0           | 0000        | 0000        | 0000        | 0000        |
| 1       | 1           | 0001        | 0001        | 0001        | 0001        |
| 2       | 2           | 0010        | 0010        | 0010        | 0010        |
| 3       | 3           | 0011        | 0011        | 0011        | 0011        |
| 4       | 4           | 0100        | 0100        | 0100        | 0100        |
| 5       | 5           | 0101        | 0101        | 0101        | 0101        |
| 6       | 6           | 0110        | 0110        | 0110        | 0110        |
| 7       | 7           | 0111        | 0111        | 0111        | 0111        |
| 8       | 8           | 1000        | 1000        | 1000        | 1000        |
| 9       | 9           | 1001        | 1001        | 1001        | 1001        |
| 10      | A           | 1010        | 1010        | 1010        | 1010        |
| 11      | B           | 1011        | 1011        | 1011        | 1011        |
| 12      | C           | 1100        | 1100        | 1100        | 1100        |
| 13      | D           | 1101        | 1101        | 1101        | 1101        |
| 14      | E           | 1110        | 1110        | 1110        | 1110        |
| 15      | F           | 1111        | 1111        | 1111        | 1111        |

In der Tabelle fällt auf, dass die Binärwerte in den Kolonnen 3 bis 6 aufsteigend von 0 bis 15 angeordnet sind. Jeder Binärwert ist eine 4-Bit-Binärzahl, was bedeutet, dass jede Stelle in den Binärwerten entweder eine 0 oder eine 1 ist und die Zahlen von 0 bis 15 jeweils durch die Kombination dieser Stellen dargestellt werden.


### Aufgabe 2

911 / 2 = 455 Rest 1
455 / 2 = 227 Rest 1
227 / 2 = 113 Rest 1
113 / 2 = 56  Rest 1
56  / 2 = 28  Rest 0
28  / 2 = 14  Rest 0
14  / 2 = 7   Rest 0
7   / 2 = 3   Rest 1
3   / 2 = 1   Rest 1
1   / 2 = 0   Rest 1

Die Binärzahl für 911 lautet also: 1110001111.


### Aufgabe 3

(1 * 2^7) + (0 * 2^6) + (1 * 2^5) + (1 * 2^4) + (0 * 2^3) + (1 * 2^2) + (1 * 2^1) + (0 * 2^0)
= 128 + 0 + 32 + 16 + 0 + 4 + 2 + 0
= 182


### Aufgabe 4

1110 -> E
0010 -> 2
1010 -> A
0101 -> 5


### Aufgabe 5

  1101'1001
"+" 0111'0101
------------
 10001'1110

Resultat: 0001'1110


### Aufgabe 6

a. Der binäre Wert 1100'0000.1010'1000.0100'1100.1101'0011 könnte eine IP-Adresse im IPv4-Format darstellen, wenn sie in dezimale Punktnotierung umgewandelt wird. Es würde dann der Form XXX.XXX.XXX.XXX entsprechen, wobei jede Gruppe von 8 Bits (Oktett) einen dezimalen Wert zwischen 0 und 255 darstellt.

b. Der binäre Wert 1011'1110-1000'0011-1000'0101-1101'0101-1110'0100-1111'1110 könnte eine MAC-Adresse (Media Access Control) darstellen, wenn sie in hexadezimale Notierung umgewandelt wird. MAC-Adressen bestehen aus 6 Oktetten (48 Bits) und werden üblicherweise in hexadezimaler Notierung dargestellt, wobei jedes Oktett durch zwei hexadezimale Ziffern repräsentiert wird.


### Aufgabe 7

Die Zeile chmod 751 CreateWeeklyReport in einem Bash-Skript könnte bedeuten, dass die Dateiberechtigungen für die Datei "CreateWeeklyReport" geändert werden. 


### Aufgabe 8

Um 107 Gondeln zu zählen, benötigen Sie mindestens 7 Bits im Binärcode, da 2^7 (128) die nächstgrößere Potenz von 2 ist, die größer oder gleich 107 ist. Daher wäre die Codebreite des Binärcodes für die Kabinenzählung 7 Bits.


### Aufgabe 9

Speicherkapazität in Byte = 2^12 * 2^16 = 2^(12+16) = 2^28
Speicherkapazität in kiB = 2^28 / 1024 = 2^(28-10) = 2^18 kiB


### Aufgabe 10

a. Anzahl der Bytes pro Sekunde = (1.000.000 * 1) / 8 = 125.000 Bytes/s

b. Anzahl der Bytes pro Sekunde (bei paralleler Verbindung) = 1.000.000 Hz = 1.000.000 Bytes/s


### Aufgabe 11

a.
Kleinster Binärwert (unsigned): 0000'0000
Größter Binärwert (unsigned): 1111'1111
Kleinster Dezimalwert (unsigned): 0
Größter Dezimalwert (unsigned): 255

b.
Kleinster Binärwert (signed): 1000'0000
Größter Binärwert (signed): 0111'1111
Kleinster Dezimalwert (signed): -128
Größter Dezimalwert (signed): 127

c. Dezimalzahl +83 in vorzeichenbehafteten Binärwert umwandeln (signed):
Dezimal: +83
Binär: 0101'0011

d. Dezimalzahl -83 in vorzeichenbehafteten Binärwert umwandeln (signed mit Zweierkomplement):
Dezimal: -83
Binär: 1010'1101

e. Addition der beiden Binärwerte:
0101'0011 (83, positiv) +
1010'1101 (-83, negativ, Zweierkomplement) =
0000'0000 (0)

f. Dezimalzahl 0 in vorzeichenbehafteten Binärwert umwandeln (signed):
Dezimal: 0
Binär: 0000'0000
Ja, die vorangegangene Addition hat auch diesen Binärwert ergeben.

g. Bei einer Datenbusbreite von 1 Byte kann die Dezimalzahl +150 nicht in einen vorzeichenbehafteten Binärwert umgewandelt werden, da der Bereich für signed integers nur -128 bis +127 abdeckt. Die Zahl +150 liegt außerhalb dieses Bereichs. Dies unterstreicht die Wichtigkeit, den passenden Datentyp mit der richtigen Größe zu wählen, um Daten angemessen darzustellen und Überläufe oder Unterläufe zu vermeiden.


### Aufgabe 12

Für eine einfache Single-Precision Fliesskommazahl könnte die Darstellung wie folgt aussehen:

1 Bit für das Vorzeichen (0 für positiv, 1 für negativ)
8 Bits für den Exponenten (mit Bias)
23 Bits für die Mantisse (Signifikand)


### Aufgabe 13

a. Logisch UND/AND (mit zwei Eingangs- und einer Ausgangsvariablen):

| Eingang A | Eingang B | Ausgang |
|-----------|-----------|---------|
|     0     |     0     |    0    |
|     0     |     1     |    0    |
|     1     |     0     |    0    |
|     1     |     1     |    1    |


b. Logisch ODER/OR (mit zwei Eingangs- und einer Ausgangsvariablen):

| Eingang A | Eingang B | Ausgang |
|-----------|-----------|---------|
|     0     |     0     |    0    |
|     0     |     1     |    1    |
|     1     |     0     |    1    |
|     1     |     1     |    1    |


c. Logisch NICHT/NOT (mit einer Eingangs- und einer Ausgangsvariablen):

| Eingang | Ausgang |
|---------|---------|
|    0    |    1    |
|    1    |    0    |


d. Logisch EXOR (mit zwei Eingangs- und einer Ausgangsvariablen):

| Eingang A | Eingang B | Ausgang |
|-----------|-----------|---------|
|     0     |     0     |    0    |
|     0     |     1     |    1    |
|     1     |     0     |    1    |
|     1     |     1     |    0    |


### Aufgabe 14

a. 11 % 2 = 1
<br>
b. 10 % 2 = 0
<br>
c. 10 % 3 = 1
<br>
d. 10 % 5 = 0
<br>
e. 10 % 9 = 1



# Reflexion zum Tag

## Ziele/Resultate:
- Verständnis für binäre Zahlensysteme und deren Anwendung in der Informatik.
- Kenntnis wichtiger logischer Funktionen und deren Wahrheitstabellen.
- Erfahrung im Umgang mit dem Modulo-Operator zur Berechnung von Restwerten.
  
## Knacknüsse:
- Verstehen der Konzepte hinter dem Zweierkomplement für die Darstellung negativer Zahlen.
- Identifizieren der korrekten Datentypen für die gegebene Aufgabenstellung.
  
## Tools:
- Markdown für die Erstellung von Dokumenten.
- Einfache Texteditoren für das Schreiben von Code und Notizen.
  
## Kommandos:
- Verwendung von Markdown-Syntax für die Erstellung von Tabellen und Textformatierung.
- Verwenden von einfachen Rechenoperationen für binäre Umwandlungen und Moduloberechnungen.