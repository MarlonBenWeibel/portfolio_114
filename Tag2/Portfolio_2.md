## A_DATEN_CODIEREN_TXT_ANNEX


### Aufgabe 1

Die ASCII-Tabelle ist als PDF angehängt.


### Aufgabe 2

https://symbl.cc/de/unicode/table/#basic-latin


### Aufgabe 3

a. 
Textsample1 = ASCII
Textsample2 = UTF-8
Textsample3 = UTF-16 BE-BOM

b.
Textsample1 = 68 Zeichen
Textsample2 = 71 Zeichen
Textsample3 = 71 Zeichen

c.
Textsample1 = 68 Byte
Textsample2 = 71 Byte
Textsample3 = 144 Byte

Die Unterschiede erklären sich durch die unterschiedlichen Codierungsformate. ASCII verwendet eine einzelne Byte-Codierung für jedes Zeichen, während UTF-8 eine variable Byte-Codierung verwendet, bei der einige Zeichen mehrere Bytes benötigen können. UTF-16 verwendet normalerweise zwei Bytes pro Zeichen, aber mit dem BOM (Byte Order Mark) werden zusätzliche Bytes am Anfang der Datei hinzugefügt, um die Byte-Reihenfolge anzugeben. Daher sind die UTF-16-Dateien größer als die ASCII- oder UTF-8-Dateien.

d.
![Alt text](Datei_1.png)
![Alt text](Datei_2.png)

Unterschiedliche Zeichen: Beim ersten HEX-Dump e4 und beim zweiten wird es so codiert c3 a4, Beim ersten HEX-Dump 80 und beim zweiten wird es so codiert e2/82/ac 

e.
Big-Endian (BE) bedeutet, dass das wichtigste Byte zuerst kommt.
Little-Endian (LE) bedeutet, dass das am wenigsten wichtige Byte zuerst kommt.

f.
Wenn ich von ASCII zu UTF-8 wechsle, werden spezielle Zeichen wie Währungssymbole und nicht-ASCII-Zeichen mit mehreren Bytes codiert. UTF-8 verwendet dabei zusätzliche Bytes, um diese Zeichen darzustellen. In Ihrem Beispiel wird das Euro-Zeichen in UTF-8 mit mehreren Bytes dargestellt, was dazu führen kann, dass es im Text anders aussieht als im ASCII-Format.


### Aufgabe 4

Ich habe ein kurzer Satz durch ein QR-Code abgebildet und mit Davi ausgetauscht. Der QR-Code konnte gelesen werden.


### Aufgabe 5

Um das Ticket fälschungssicher zu machen, wurde eine zusätzliche Prüfziffer oder Checksumme hinzugefügt, um die Gültigkeit des Tickets zu überprüfen. Dies wurde durch die Verwendung eines kryptografischen Hash-Algorithmus erreicht, der die codierten Informationen im QR-Code überprüft.

Der Datensatz wurde in einen QR-Code abgebildet, indem die Informationen in einem strukturierten Format codiert wurden, das von QR-Code-Generatoren unterstützt wird. Jeder Abschnitt des QR-Codes enthält die entsprechenden Informationen wie Datum und Uhrzeit der Veranstaltung, Ticketnummer, Sitzplatznummer, Tribünensektor und Besucher-ID.

Die QR-Codes können von einem QR-Code-Lesegerät am Stadioneingang gescannt werden, um die Informationen zu extrahieren und mit einer Datenbank abzugleichen. Auf diese Weise kann der Platzanweiser die Personalie und Gültigkeit des Tickets überprüfen und den Besucher entsprechend in den richtigen Stadionsektor leiten.







# Reflexion zum Tag

## Ziele/Resultate:
- Intensive Beschäftigung mit verschiedenen Themen im Bereich der Datenkodierung.
- Verständnis der Unterschiede zwischen verschiedenen Kodierungsformaten wie ASCII, UTF-8, UTF-16.
- Erfolgreiche Lösung von Aufgaben zu QR-Codes und anderen Komprimierungsalgorithmen.
  
## Knacknüsse:
- Verständnis komplexer Konzepte wie der Burrows-Wheeler-Transformation und dem LZW-Verfahren.

  
## Tools:
- Notepad
- Symbl.cc (für Unicode-Tabelle)
- QR-Code-Generatoren
  
## Kommandos:
- Keine spezifischen Kommandos verwendet.
