## D_KRYPTOGRAFIE_SYM_ANNEX


### Aufgabe 1
 
CrypTool1 installiert



### Aufgabe 2

«DER ANGRIFF ERFOLGT ZUR TEEZEIT
DIE WUERFEL SIND GEFALLEN
ICH KAM SAH UND SIEGTE
TEILE UND HERRSCHE»




### Aufgabe 3

B + A = B
E + F = J
E + F = J
F + E = J
Also ergibt "BEEF" verschlüsselt mit dem Schlüssel "AFFE" den Geheimtext "BJJJ".

W - S = E
R - E = N
K - C = I
X - R = G
Q - E = M
T - T = A
Daher lautet die Entschlüsselung des Geheimtexts "WRKXQT" mit dem Schlüsselwort "SECRET" "ENIGMA".

"DER STAAT BIN ICH
ES IST AEUSSERST SCHWIERIG ZU REDEN OHNE VIEL ZU SAGEN
ICH MACHE MIT JEDER ERNENNUNG NEUNUNDNEUNZIG UNZUFRIEDENE UND
EINEN UNDANKBAREN
LOUIS XIV"


### Aufgabe 4

Um die Dezimalzahl 4711 mit einer XOR-Stromchiffre zu verschlüsseln, müssen wir sie zuerst in eine 16-Bit-Binärzahl umwandeln:

4711 in binär: 0001'0010'0110'0111

Der binäre Schlüssel lautet: 1000'1101

Da der Schlüssel kürzer als die Daten ist, wird er mehrmals wiederholt, um den gesamten Datenstrom abzudecken:

Schlüssel (wiederholt): 1000'1101'1000'1101

Jetzt führen wir eine XOR-Operation zwischen der binären Darstellung von 4711 und dem wiederholten Schlüssel durch:

0001'0010'0110'0111 XOR 1000'1101'1000'1101 = 1001'1111'1110'1010

Die verschlüsselte Chiffre lautet also: 1001'1111'1110'1010

Um die Chiffre zu entschlüsseln, führen wir erneut eine XOR-Operation zwischen der Chiffre und dem wiederholten Schlüssel durch. Das Ergebnis sollte die ursprüngliche binäre Darstellung von 4711 sein.


### Aufgabe 5

Passwort überprüft






## D_KRYPTOGRAFIE_ASYM_ANNEX


 
Auf CrypTool1 die Aufgaben durchgemacht (ausser optionale)




# Reflexion zum Tag

## Ziele/Resultate:
- Erfolgreiche Bearbeitung von Aufgaben im Bereich der symmetrischen und asymmetrischen Kryptografie.
- Klare Darstellung und Anwendung verschiedener Verschlüsselungsverfahren wie Caesar-Chiffre, Vigenère-Verschlüsselung, XOR-Stromchiffre, Diffie-Hellman-Protokoll und RSA.
- Verständnis für die Funktionsweise und Anwendungsbereiche verschiedener Kryptografie-Algorithmen entwickelt.
- Experimente mit verschiedenen Verschlüsselungswerkzeugen in Cryptool durchgeführt.

  
## Knacknüsse:
- Sicherstellung der korrekten Anwendung der Verschlüsselungsverfahren, insbesondere bei komplexeren Algorithmen wie RSA und Diffie-Hellman.
- Verständnis für die Auswirkungen von Sicherheitslücken und Schwachstellen in Kryptografie-Algorithmen, insbesondere im Hinblick auf Hash-Verfahren und digitale Signaturen.


  
## Tools:
- Verwendung von Cryptool für praktische Experimente und Anwendungen im Bereich der Kryptografie.
- Notizen und Überlegungen zur Dokumentation von Verschlüsselungsmethoden und Ergebnissen.
  
## Erkenntnisse:
- Keine Unterschiede zwischen symmetrischen und asymmetrischen Verschlüsselungsverfahren klar herausgearbeitet.
 Kommandos verwendet.
- Wichtigkeit von Sicherheitsaspekten und potenziellen Angriffsszenarien in der Kryptografie besser verstanden.
- Praktische Anwendung von Verschlüsselungstechniken zur sicheren Kommunikation und Datenübertragung.
